#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int main()
{
    int nr = 0;
    char surname[20] = "";
    int p = 1;

    struct Client *head;
    head = (struct Client *)malloc(sizeof(struct Client));
    strcpy(head->surname, "Kowalski");
    head->next = NULL;

    while (p == 1)
    {
        printf("1. Dodaj nowego kilenta na koniec\n2. Usun klineta o danym nazwisku\n3. Wyswietl ilosc klientow\n4. Wyswietl nazwiska klinetow\n");

        scanf("%d", &nr);

        switch (nr)
        {
        case 1:
            printf("Podaj nazwisko klienta:\n");
            scanf("%s", surname);
            push_back(&head, surname);
            break;

        case 2:
            printf("Podaj nazwisko klienta:\n");
            scanf("%s", surname);
            pop_by_surname(&head, surname);
            break;

        case 3:
            printf("%d", list_size(head));
            break;

        case 4:
            show_list(head);
            break;

        default:
            break;
        }

        printf("Jesli chcesz zakonczyc to kliknij 0, a jesli chcesz kontynulowac 1\n");
        scanf("%d", &p);

    }

    return 0;
}